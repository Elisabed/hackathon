class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :task_id
      t.string :task_name
      t.string :task_description
      t.boolean :task_status

      t.timestamps null: false
    end
  end
end
