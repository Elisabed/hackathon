class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :job_id
      t.string :job_name
      t.string :location
      t.date :due_date
      t.integer :task_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
