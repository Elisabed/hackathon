json.array!(@employees) do |employee|
  json.extract! employee, :id, :employee_id, :employee_name
  json.url employee_url(employee, format: :json)
end
