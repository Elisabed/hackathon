json.array!(@jobs) do |job|
  json.extract! job, :id, :job_id, :job_name, :location, :due_date, :task_id, :employee_id
  json.url job_url(job, format: :json)
end
